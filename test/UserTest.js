const assert = require('assert');
const User = require('../src/app/model/User');
const HTTPError = require("../src/app/error/HTTPError");

describe('User tests', function() {
  it('User can be created', async function() {
    try {
      let userAttributes = {
        name: "name",
        surname: "surname", 
        email: "email@email.com",
        password: "password",
        birthday: "01/01/1991", 
        picture: "picture"
      }
      new User(userAttributes);
      assert.strictEqual(true, true);
    } catch (e) {
      assert.strictEqual(true, e.message);
    }
  });

  it('User email must have @', async function() {
    let email = "holahola.com";
    let userAttributes = {
      name: "name",
      surname: "surname", 
      email,
      password: "password",
      birthday: "01/01/1991", 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    } catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      } else {
        assert.strictEqual(true, false);
      }
    }
  } );

  it('User email must have only one @', async function() {
    let email = "hola@@hola.com";
    let userAttributes = {
      name: "name",
      surname: "surname", 
      email,
      password: "password",
      birthday: "01/01/1991", 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    }
    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

  it('User email must have .', async function() {
    let email = "hola@holacom";
    let userAttributes = {
      name: "name",
      surname: "surname", 
      email,
      password: "password",
      birthday: "01/01/1991", 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    }
    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

  it('User email cannot be too short', async function() {
    let email = "@.com";
    let userAttributes = {
      name: "name",
      surname: "surname", 
      email,
      password: "password",
      birthday: "01/01/1991", 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    }
    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

  it('User email cannot be too long', async function() {
    let email = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \  @b.com";
    let userAttributes = {
      name: "name",
      surname: "surname", 
      email,
      password: "password",
      birthday: "01/01/1991", 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    }
    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

  it('User name cannot be too short', async function() {
    let name = "a";
    let userAttributes = {
      name,
      surname: "surname", 
      email: "email@email.com",
      password: "password",
      birthday: "01/01/1991", 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    }

    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

  it('User name cannot be too long', async function() {
    let name = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" ;
    let userAttributes = {
      name,
      surname: "surname", 
      email: "email@email.com",
      password: "password",
      birthday: "01/01/1991", 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    }
    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

  it('User surname cannot be too short', async function() {
    let surname = "a";
    let userAttributes = {
      name: "name",
      surname, 
      email: "email@email.com",
      password: "password",
      birthday: "01/01/1991", 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    }

    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

  it('User surname cannot be too long', async function() {
    let surname = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa \ " +
      "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" ;
    let userAttributes = {
      name: "name",
      surname, 
      email: "email@email.com",
      password: "password",
      birthday: "01/01/1991", 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      assert.strictEqual(true, false);
    }
    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

  it('User name should only have letters', async function() {
    let name = "juan8";
    let userAttributes = {
      name,
      surname: "surname", 
      email: "email@email.com",
      password: "password",
      birthday: "01/01/1991", 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    }

    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

  it('User surname should only have letters', async function() {
    let surname = "juan8";
    let userAttributes = {
      name: "name",
      surname, 
      email: "email@email.com",
      password: "password",
      birthday: "01/01/1991", 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    }

    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

  it('Dates must have the correct format', async function() {
    let birthday = "01/011991";
    let userAttributes = {
      name: "name",
      surname: "surname", 
      email: "email@email.com",
      password: "password",
      birthday, 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    }

    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

  it('Dates must be valid', async function() {
    let birthday = "32/01/1991";
    let userAttributes = {
      name: "name",
      surname: "surname", 
      email: "email@email.com",
      password: "password",
      birthday, 
      picture: "picture"
    }
    try {
      new User(userAttributes);
      throw new Error("Test failed");
    }

    catch (err) {
      if (err instanceof HTTPError) {
        assert.strictEqual(true, true);
      }

      else {
        assert.strictEqual(true, false);
      }
    }
  });

} );