FROM node:14
WORKDIR /rest_excercise

COPY package.json ./
COPY package-lock.json ./
RUN npm install nodemon -g
RUN npm install

COPY . ./
CMD npm start
