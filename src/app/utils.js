const HTTPError = require('./error/HTTPError');
const dotenv = require('dotenv').config();
const crypto = require('crypto');
const encode = require('nodejs-base64-encode');
const jwt = require('jsonwebtoken');
const e = require('cors');

function invalidStringLength(s, minLen, maxLen) {
  return (s.length < minLen || s.length > maxLen);
}

function checkStringLength(s, minLen, maxLen, errMsg) {
  if ( s === undefined || invalidStringLength(s, minLen, maxLen) )
    throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, errMsg);
}

function invalidEmail(email) {
  return (email.length < 6 ||
    ( email.split("@").length - 1 ) !== 1 ||
    !email.includes(".") ||
    email.length > process.env.MAX_STR_LEN);
}

function checkInvalidEmail(email) {
  if ( invalidEmail(email) )
    throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, process.env.INVALID_EMAIL);
}

function isAlphabeticString(s, errMsg) {
  if ( !s.match(/^[a-zA-Z]+$/i) )
    throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, errMsg);
}

function respond(res, status, res_body) {
  console.log(`Response status: ${status}\n` +
    `\tResponse body: ${JSON.stringify(res_body)}`);
  res.status(status).json(res_body);
}

function base64encode(string) {
  return encode.encode(string, 'base64');
}

function base64decode(string) {
  return encode.decode(string, 'base64');
}

function getHash(toHash) {
  return crypto.createHmac('md5', toHash).digest('hex');
}

function getNonAmericanDate(date) {
  let numbers = date.split("/");
  return numbers[1] + "/" + numbers[0] + "/" + numbers[2]
}

function checkDate(birthday) {
  if ( new Date( getNonAmericanDate(birthday) ).toString()
    === "Invalid Date" ) {
    throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, process.env.INVALID_DATE);
  }
}

function filterToken(tokenValue){
  if ( tokenValue === undefined )
    throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, process.env.TOKEN_INVALID);

    let arrAux = tokenValue.split(" ");
    return arrAux[1];
}

function jwtSign(id) {
  return jwt.sign({ id: id }, process.env.JWT_SECRET,
    { expiresIn: process.env.JWT_EXPIRE_SECONDS });
}

function jwtVerify(tokenValue) {
  return jwt.verify(tokenValue, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      if (err.name === 'TokenExpiredError')
        throw new HTTPError(process.env.HTTP_401_UNAUTHORIZED, process.env.TOKEN_EXPIRED);
      else
        throw new HTTPError(process.env.HTTP_401_UNAUTHORIZED, process.env.TOKEN_INVALID);
    }
    return decoded;
  });
}

exports.checkStringLength = checkStringLength;
exports.isAlphabeticString = isAlphabeticString;
exports.invalidEmail = invalidEmail;
exports.checkInvalidEmail = checkInvalidEmail;
exports.respond = respond;
exports.getHash = getHash;
exports.base64encode = base64encode;
exports.base64decode = base64decode;
exports.checkDate = checkDate;
exports.getNonAmericanDate = getNonAmericanDate;
exports.filterToken = filterToken;
exports.jwtSign = jwtSign;
exports.jwtVerify = jwtVerify;
