const User = require('../model/User');

class UserController {
  constructor(userAttributes, image) {
    this.userModel = new User(userAttributes, image);
  }

  toJSON() {
    return this.userModel.toJSON();
  }

  getImage(){
    return this.userModel.getImage();
  }

  checkCredentials(email, password) {
    return this.userModel.checkCredentials(email, password);
  }

  update(toUpdate) {
    this.userModel.update(toUpdate);
  }

  setImage(image) {
    this.userModel.setImage(image);
  }
}

module.exports = UserController;
