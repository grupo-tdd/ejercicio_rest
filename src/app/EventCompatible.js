const utils = require('./utils');

class EventCompatible {
  handleEventError(res, err) {
    let status = err.status || process.env.HTTP_500_INTERNAL_SERVER_ERROR;
    if (status === process.env.HTTP_500_INTERNAL_SERVER_ERROR)
      console.log(err.stack);
    utils.respond(res, status, { error: err.message });
  }
}

module.exports = EventCompatible;
