const cors = require('cors');
const dotenv = require('dotenv').config();
const utils = require('./utils');
const express = require('express');
const bodyParser = require('body-parser');
const multer = require("multer");
const path = require("path");

const EventCompatible = require("./EventCompatible");
const UserManager = require("./web/UserManager");

const storage = multer.diskStorage({
    destination:path.join(__dirname,"/public/images"),
    filename: (req,file,callback) =>{
      callback(null,Date.now() + "." + file.originalname);
    }
});

class Main extends EventCompatible {
  constructor() {
    super();
    this.app = express();
    this.app.use( cors() );

    this.app.use( multer(
      { storage: storage,
        dest: path.join(__dirname,"/public/images") }
    ).single("image") );

    this.app.use( express.static( path.join(__dirname,"/public") ) );

    this.app.use( bodyParser.json() );
    this.app.listen(process.env.NODE_DOCKER_PORT, () => {
      console.log(`Listening on port ${process.env.NODE_DOCKER_PORT}`)
    });

    this.userManager = new UserManager();
  }

  defineEvents() {
    this.userManager.defineEventsFor(this.app);
  }

}

const backend = new Main();
backend.defineEvents();
