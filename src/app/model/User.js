const utils = require('../utils');
const HTTPError = require('../error/HTTPError');
const fs = require("fs");

class User {
  constructor(attrs, image) {
    this.checkAttrs = {
      name: this.checkName.bind(this),
      surname: this.checkSurname.bind(this),
      email: this.checkEmail.bind(this),
      password: this.checkPassword.bind(this),
      birthday: this.checkBirthday.bind(this)
    };

    this.setAttrs = {
      name: this.setName.bind(this),
      surname: this.setSurname.bind(this),
      email: this.setEmail.bind(this),
      password: this.setPassword.bind(this),
      birthday: this.setBirthday.bind(this)
    };

    this.picture = image;

    Object.keys(this.setAttrs).forEach(attr => {
      this.setAttrs[attr](attrs[attr]);
    });
  }

  checkName(name) {
    utils.checkStringLength(name,
      process.env.MIN_STR_LEN,
      process.env.MAX_STR_LEN,
      "Longitud de nombre inválida, deberia tener entre "+
      `[${process.env.MIN_STR_LEN}, ${process.env.MAX_STR_LEN}].`);
    utils.isAlphabeticString(name, "Nombre inválido.");
  }

  checkSurname(surname) {
    utils.checkStringLength(surname,
      process.env.MIN_STR_LEN,
      process.env.MAX_STR_LEN,
      `Longitud de apellido inválida, deberia tener entre  \
      [${process.env.MIN_STR_LEN}, ${process.env.MAX_STR_LEN}]`);
    utils.isAlphabeticString(surname, "Apellido inválido.");
  }

  checkEmail(email) {
    utils.checkInvalidEmail(email);
  }

  checkPassword(password) {
    utils.checkStringLength(password,
      process.env.MIN_PASS_LEN,
      process.env.MAX_STR_LEN,
      `Longitud invalida del atributo password, deberia tener entre  \
      [${process.env.MIN_PASS_LEN}, ${process.env.MAX_STR_LEN}]`);
  }

  checkBirthday(birthday) {
    utils.checkDate(birthday);
  }

  setName(name) {
    this.checkAttrs['name'](name);
    this.name = name;
  }

  setSurname(surname) {
    this.checkAttrs['surname'](surname);
    this.surname = surname;
  }

  setEmail(email) {
    this.checkAttrs['email'](email);
    this.email = email;
  }

  setPassword(password, passwordHashed = false) {
    this.checkAttrs['password'](password);
    this.password = passwordHashed ? password : utils.getHash(password);
  }

  setBirthday(birthday) {
    this.checkAttrs['birthday'](birthday);
    this.birthday = new Date( utils.getNonAmericanDate(birthday) );
  }

  setPicture(picture) {
    this.checkAttrs['picture'](picture);
    this.picture = picture;
  }

  toJSON() {
    return { name: this.name,
      surname: this.surname,
      birthday: this.birthday,
      email: this.email
    }; 
  }

  checkCredentials(email, password) {
    if ( this.email !== email || this.password !== utils.getHash(password) )
      throw new HTTPError(process.env.HTTP_401_UNAUTHORIZED, process.env.INVALID_LOGIN_INFO);
  }

  update(toUpdate) {
    Object.keys(toUpdate).forEach(attr => {
      if (this.checkAttrs[attr])
        this.checkAttrs[attr](toUpdate[attr]);
    });
    Object.keys(toUpdate).forEach(attr => {
      if (this.checkAttrs[attr])
        this.setAttrs[attr](toUpdate[attr]);
    });
  }

  getImage(){
    return this.picture;
  }

  setImage(image){
    let auxImage = this.picture;
    fs.unlinkSync(auxImage.path);
    this.picture = image;
  }

}

module.exports = User;
