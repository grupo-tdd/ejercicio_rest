const UserController = require('../controller/UserController');
const EventCompatible = require("../EventCompatible");
const utils = require("../utils");
const HTTPError = require("../error/HTTPError");
const jwt = require('jsonwebtoken');
const path = require("path");

class UserManager extends EventCompatible {
  constructor() {
    super();
    this.users = {};
    this.userIds = {};
    this.userCount = 0;
  }

  defineEventsFor(app) {
    app.post(process.env.USER_URI, this.createUser.bind(this));
    app.post(process.env.LOG_IN_URI, this.logInUser.bind(this));
    app.get(process.env.USER_URI, this.getUserDefinition.bind(this));
    app.get(process.env.USER_URI + process.env.PROFILE_PICTURE, this.getProfilePicture.bind(this));
    app.patch(process.env.USER_URI, this.updateUser.bind(this));
    app.delete(process.env.USER_URI, this.deleteUser.bind(this));
  }

  async createUser(req, res) {
    console.log(`POST ${process.env.USER_URI}`);
    try {
      const body = Object.assign({},req.body);
      this.checkUserIsNew(body.email);
      let user = new UserController(body, req.file);
      this.userCount += 1;
      this.userIds[body.email] = this.userCount;
      console.log(this.userIds);
      this.users[this.userCount] = user;
      utils.respond( res, process.env.HTTP_201_CREATED, user.toJSON() );
    } catch (err) {
      this.handleEventError(res, err);
    }
  }

  logInUser(req, res) {
    console.log(`POST ${process.env.LOG_IN_URI}`);
    try {
      if (!req.headers.authorization)
        throw new HTTPError(process.env.HTTP_401_UNAUTHORIZED, process.env.TOKEN_NOT_FOUND);
      let filteredToken = utils.filterToken(req.headers.authorization);
      let plainAuth = utils.base64decode(filteredToken);
      let email = plainAuth.split(':')[0];
      let password = plainAuth.split(':')[1];
      this.checkUserExists(this.userIds[email]);
      this.users[this.userIds[email]].checkCredentials(email, password);
      let token = utils.jwtSign(this.userIds[email]);

      utils.respond(res, process.env.HTTP_201_CREATED, {token});
    } catch (err) {
        this.handleEventError(res, err);
    }
  }

  getUserDefinition(req, res) {
    console.log(`GET ${process.env.USER_URI}`);
    try {
        let tokenValue = utils.filterToken(req.headers.authorization);
        console.log(tokenValue);
        let user = this.verifyUser(tokenValue);
        utils.respond(res, process.env.HTTP_200_OK, user.toJSON());
          
      } catch (err) {
        this.handleEventError(res, err);
    }
  }

  getProfilePicture(req, res) {
    console.log(`GET ${process.env.USER_URI}${process.env.PROFILE_PICTURE} `);
    try{
        let tokenValue = utils.filterToken(req.headers.authorization);
        let user = this.verifyUser(tokenValue);
        console.log(user);
        res.sendFile(user.getImage().path);
    }
    catch (err){
      this.handleEventError(res, err);
    }
  }

  updateUser(req, res) {
    console.log(`PATCH ${process.env.USER_URI}`);
    try {
      const body = Object.assign({},req.body);
      let tokenValue = utils.filterToken(req.headers.authorization);
      let userId = utils.jwtVerify(tokenValue).id;
      this.checkUserExists(userId);
      let previousEmail = this.users[userId].toJSON().email;
      this.users[userId].update(body);

      if (body.email)
        this.userIds[previousEmail] = undefined;
        this.userIds[body.email] = userId;
      
      console.log(previousEmail);

      if( req.file ) {
        this.users[userId].setImage(req.file);
      }

      utils.respond(res, process.env.HTTP_200_OK, this.users[userId].toJSON());
      
    } catch (err) {
        this.handleEventError(res, err);
    }
  }

  deleteUser(req, res) {
    console.log(`DELETE ${process.env.USER_URI}`);
    try {
      let tokenValue = utils.filterToken(req.headers.authorization);
      let userId = utils.jwtVerify(tokenValue).id;
      this.checkUserExists(userId);
      let user = this.users[userId];
      let userJson = user.toJSON();
      delete this.userIds[userJson.email];
      delete this.users[userId];
      utils.respond(res, process.env.HTTP_200_OK, userJson);
    } catch (err) {
        this.handleEventError(res, err);
    }
  }

  checkUserIsNew(email) {
    if (this.userIds[email])
      throw new HTTPError(process.env.HTTP_400_BAD_REQUEST, process.env.USER_NOT_NEW);
  }

  checkUserExists(userId) {
    if (!this.users[userId])
      throw new HTTPError(process.env.HTTP_404_NOT_FOUND, process.env.USER_NOT_FOUND);
  }

  verifyUser(tokenValue) {
    let userId = jwt.verify(tokenValue, process.env.JWT_SECRET).id;
    this.checkUserExists(userId);
    return this.users[userId];
  }
}

module.exports = UserManager;
